<?php

namespace Mova\DevOpsExample\Service;

use DateTime;
use DateTimeZone;

class TimeFormatter
{
    public function inTimezone(DateTime $time, DateTimeZone $dtz): string
    {
        $time->setTimezone($dtz);

        return $time->format(DateTime::ISO8601);
    }
}
