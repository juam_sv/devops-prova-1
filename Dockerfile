FROM marciodojr/php-fpm:81

COPY --chown=intec:intec . /var/www/app

RUN composer install --no-ansi --no-dev --no-interaction --no-progress --no-scripts --optimize-autoloader

CMD ["php", "public/index.php"]